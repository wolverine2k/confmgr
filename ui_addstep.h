/********************************************************************************
** Form generated from reading UI file 'addstep.ui'
**
** Created: Thu Nov 11 12:46:30 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDSTEP_H
#define UI_ADDSTEP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddStep
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *addStepValue;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *addStepDelay;
    QLabel *label_4;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *btnaddStep;
    QPushButton *btnCancel;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;

    void setupUi(QWidget *AddStep)
    {
        if (AddStep->objectName().isEmpty())
            AddStep->setObjectName(QString::fromUtf8("AddStep"));
        AddStep->setWindowModality(Qt::ApplicationModal);
        AddStep->resize(800, 424);
        horizontalLayoutWidget = new QWidget(AddStep);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(30, 170, 741, 71));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        addStepValue = new QLineEdit(horizontalLayoutWidget);
        addStepValue->setObjectName(QString::fromUtf8("addStepValue"));
        addStepValue->setInputMethodHints(Qt::ImhDialableCharactersOnly);

        horizontalLayout->addWidget(addStepValue);

        horizontalLayoutWidget_2 = new QWidget(AddStep);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(31, 250, 281, 71));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(horizontalLayoutWidget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        addStepDelay = new QLineEdit(horizontalLayoutWidget_2);
        addStepDelay->setObjectName(QString::fromUtf8("addStepDelay"));
        addStepDelay->setInputMethodHints(Qt::ImhDigitsOnly);

        horizontalLayout_2->addWidget(addStepDelay);

        label_4 = new QLabel(horizontalLayoutWidget_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_2->addWidget(label_4);

        horizontalLayoutWidget_3 = new QWidget(AddStep);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(30, 330, 741, 71));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        btnaddStep = new QPushButton(horizontalLayoutWidget_3);
        btnaddStep->setObjectName(QString::fromUtf8("btnaddStep"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnaddStep->sizePolicy().hasHeightForWidth());
        btnaddStep->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(btnaddStep);

        btnCancel = new QPushButton(horizontalLayoutWidget_3);
        btnCancel->setObjectName(QString::fromUtf8("btnCancel"));
        sizePolicy.setHeightForWidth(btnCancel->sizePolicy().hasHeightForWidth());
        btnCancel->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(btnCancel);

        verticalLayoutWidget = new QWidget(AddStep);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(30, 9, 741, 151));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setWordWrap(true);

        verticalLayout->addWidget(label_3);


        retranslateUi(AddStep);
        QObject::connect(btnCancel, SIGNAL(clicked()), AddStep, SLOT(close()));
        QObject::connect(btnaddStep, SIGNAL(clicked()), AddStep, SLOT(AddOperationStep()));

        QMetaObject::connectSlotsByName(AddStep);
    } // setupUi

    void retranslateUi(QWidget *AddStep)
    {
        AddStep->setWindowTitle(QApplication::translate("AddStep", "AddStep", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AddStep", "Value:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("AddStep", "Delay:", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("AddStep", "Seconds", 0, QApplication::UnicodeUTF8));
        btnaddStep->setText(QApplication::translate("AddStep", "Add Step", 0, QApplication::UnicodeUTF8));
        btnCancel->setText(QApplication::translate("AddStep", "Cancel Operation", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("AddStep", "The Value field is a phone number or DTMF tones. Include characters like *, + or # in the Value as and when they are required. The Delay in seconds shows the time allowed to lapse before dialing the corresponding value. The first step typically has 0 delay with the conference number as a value.", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AddStep: public Ui_AddStep {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDSTEP_H
