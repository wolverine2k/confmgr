#ifndef PROFILE_H
#define PROFILE_H

#include <QObject>
#include <QList>

class Steps
{
public:
    Steps() { }
    Steps(const QString &value, const int &delay) : mValue(value), mDelay(delay) {}
    ~Steps() { }

    Steps(const Steps &s) {
        *this = s;
    }

    Steps& operator=(const Steps &s) {
        if(this != &s)
        {
            mValue = s.mValue;
            mDelay = s.mDelay;
        }
        return *this;
    }

    // Getters...
    QString value() { return mValue; }
    int delay() { return mDelay; }

    // Setters...
    void setValue(const QString &value) { mValue = value; }
    void setDelay(const int &delay) { mDelay = delay; }

private:
    QString mValue;
    int mDelay;
};

class Profile : public QObject
{
    Q_OBJECT
public:
    explicit Profile(QObject *parent = 0);
    Profile(const Profile &p);
    ~Profile();

    Profile& operator=(const Profile &p);

    QList<Steps> mSteps;

    QString mXMLString;
    QString mName;
    unsigned int mNoOfSteps;     // assumed number of steps no more then 255 and always positive!

signals:

public slots:

};

#endif // PROFILE_H
