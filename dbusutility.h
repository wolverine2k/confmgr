/*
@version: 0.1
@author: Sudheer K. <scifi.guy@hotmail.com>
@license: GNU General Public License
*/

#ifndef DBUSUTILITY_H
#define DBUSUTILITY_H

#include <QDBusMessage>
#include <QDBusConnection>

#define NOTIFICATION_SERVICE    "org.freedesktop.Notifications"
#define NOTIFICATION_PATH       "/org/freedesktop/Notifications"
#define NOTIFICATION_INTERFACE  "org.freedesktop.Notifications"

class DbusUtility
{
protected:
    QDBusConnection connection;
public:
    DbusUtility():connection(QDBusConnection::systemBus())
    {}
    ~DbusUtility();
    QDBusConnection getConnection();
    void setConnection(QDBusConnection connection);
    bool sendSignal(QString strPath,QString strInterface,QString strName);
    bool sendMethodCall(QString strService,QString strPath,
                        QString strInterface,QString strMethodName,
                        QList<QVariant> & arguments);
    bool displayNotification(QString strMessage);
    QString getErrorMessage();
};

#endif // DBUSUTILITY_H
