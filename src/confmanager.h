#ifndef CONFMANAGER_H
#define CONFMANAGER_H

#include "profile.h"
#include "dbusutility.h"

#define CSD_CALL_BUS_NAME "com.nokia.csd.Call"
#define CSD_CALL_INTERFACE "com.nokia.csd.Call"
#define CSD_CALL_INSTANCE_INTERFACE "com.nokia.csd.Call.Instance"
#define CSD_CALL_CONFERENCE "com.nokia.csd.Call.Conference"
#define CSD_CALL_PATH "/com/nokia/csd/call"
#define CSD_CALL_INSTANCE_PATH "/com/nokia/csd/call/1"
#define CSD_CALL_SERVICE "com.nokia.csd.Call"
#define CSD_SERVICE "com.nokia.csd"
#define CSD_CALL_STATUS_INTERFACE "com.nokia.csd.Call.Instance.CallStatus"  // Should be 7 for a proper connection

class confManager : public QObject
{
    Q_OBJECT
public:
    explicit confManager(QObject *parent = 0);

    void setProfile(Profile &p);
    void startConference();

public slots:
    void sendDTMF(const QDBusMessage &dBusMessage);
    void StopCallMonitors();
    void continueSendDTMF();

private:
    void StartCallMonitors();

private:
    Profile mProfileInUse;
    bool mProfileSet;
    DbusUtility mDBusUtility;
    unsigned int mInStep;
};

#endif // CONFMANAGER_H
