#include "config.h"
#include "profile.h"
#include "xmlutil.h"

#include <QDebug>
#include <QDir>

/*
 TODO: Create backup & restore functions for the backup configuration files.
     Today it only writes a backup when the removeAllProfiles() is called.
 */

Config::Config(QObject *parent) :
    QObject(parent)
{
    confFile.setFileName(CONFIG_FILE);
    noOfProfiles = 0;
    profileList.clear();
}

bool Config::openConfig()
{
    int stopLoops = 0;
    qDebug() << "Opening the Config file";

    processAgain:
    bool bOpenResult = confFile.open(QIODevice::ReadWrite | QIODevice::Text);
    if(bOpenResult)
    {
        QString errorMsg;
        int errorLine, errorColumn;
        bOpenResult = domDoc.setContent(&confFile, &errorMsg,
                                        &errorLine, &errorColumn);
        if(!bOpenResult)
        {
            qDebug() << "Error while setContent on Line: " << errorLine
                    << "Column: " << errorColumn << endl
                    << "Error Text: " << errorMsg;
//            return bOpenResult;
            confFile.write(DEFAULT_XML);
            closeConfig();
            if(0 == stopLoops)
            {
                stopLoops++;
                goto processAgain;
            }
        }

        QDomNode node = domDoc.namedItem(PROFILE_TAG);
        if(node.isNull())
        {
            qDebug() << "Probably first use of config! Creating Profiles Tag";
            QDomElement el = Xmlutil::addElement(domDoc, domDoc, PROFILE_TAG);
            // noOfProfiles should be 0, initialized in CTOR...
            el.setAttribute(NO_OF_PROFILE_ATTR, noOfProfiles);
            return true;
        }

        // Now load the whole config file in memory!
        qDebug() << "Trying to load the whole config now...";
        bOpenResult = readAllProfiles();
    }
    return bOpenResult;
}

bool Config::closeConfig()
{
    bool bResult = confFile.flush();
    confFile.close();
    return bResult;
}

int Config::addProfile(const Profile &p)
{
    int error = 0;
    QDomElement el = domDoc.namedItem(PROFILE_TAG).toElement();
    Xmlutil::generateProfileXML(p, &error, &domDoc, &el);
    if(!error)
    {
        noOfProfiles++;
        updateNoOfProfiles();
        profileList.append(p);
    }
    qDebug() << "AddProfile(): " << error << "\tXMLString is: " << endl << domDoc.toString();
    return error;
}

void Config::updateNoOfProfiles()
{
    QDomElement el = domDoc.namedItem(PROFILE_TAG).toElement();
    el.setAttribute(NO_OF_PROFILE_ATTR, noOfProfiles);
}

int Config::removeProfile(const Profile &p)
{
    QDomElement elem = domDoc.namedItem(PROFILE_TAG).toElement();
    QDomElement el = elem.firstChildElement();
    qDebug() << el.text();
    for(int i = 0; i < noOfProfiles; i++)
    {
        QString name = el.attribute(NAME_ATTR);

        qDebug() << "Name is: " << name;
        if(p.mName == name)
        {
            if(!el.parentNode().removeChild(el).isNull())
            {
                noOfProfiles--;
                updateNoOfProfiles();
                profileList.removeAt(i);
                qDebug() << "RemoveChild okay!";
            }
            break;
        }
        el = el.nextSiblingElement();
    }
    return 0;
}

void Config::flushConfig()
{
    confFile.flush();
}

bool Config::readAllProfiles()
{
    // Point to the PROFILE_TAG node->element
    QDomElement el = domDoc.namedItem(PROFILE_TAG).toElement();
    noOfProfiles = el.attribute(NO_OF_PROFILE_ATTR).toInt();

    qDebug() << "Number of profiles in config is: " << noOfProfiles;

    if(noOfProfiles > 0)
    {
        QDomElement childEl = el.firstChildElement();
        for(int i = 0; i < noOfProfiles; i++)
        {
            Profile p;
            QString dummy; dummy.clear();
            if(!Xmlutil::degenerateProfileXML(dummy, p, &childEl))
            {
                profileList.append(p);
                qDebug() << "Profile added to ProfileList...";
            }
            else
            {
                qDebug() << "Profile not added, returning false";
                return false;
            }
            childEl = childEl.nextSiblingElement();
        }
    }
    return true;
}

bool Config::writeAllProfiles()
{
    // Stupid hack to dump all the document data into the config file...
    bool bResult = closeConfig();
    QDir dir(CONFIG_DIR);
    bResult = dir.remove(CONFIG_FILE);

    bResult = confFile.open(QIODevice::WriteOnly | QIODevice::Text);

    // No need to do set content since document data is always updated...
    qint64 error = confFile.write(domDoc.toByteArray());
    flushConfig();
    if( -1 == error)
        return false;
    return true;
}

bool Config::removeAllProfiles()
{
    // Create a backup file just in case...
    profileList.clear();
    QDir dir(CONFIG_DIR);
    bool bResult = dir.remove(BACKUP_CONFIG_FILE);
    bResult = confFile.copy(BACKUP_CONFIG_FILE);
    // Essentially just delete the whole file and re-open
    bResult = closeConfig();
    bResult = dir.remove(CONFIG_FILE);
    bResult = openConfig();
    return bResult;
}
