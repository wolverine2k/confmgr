#-------------------------------------------------
#
# Project created by QtCreator 2010-06-23T14:10:47
#
#-------------------------------------------------

QT       += core gui
QT       += xml maemo5 dbus

TARGET = confmgr
TEMPLATE = app
DEPENDPATH += .
DESTDIR = ../

SOURCES += main.cpp\
        mainwindow.cpp \
    xmlutil.cpp \
    profile.cpp \
    config.cpp \
    addprofile.cpp \
    addstep.cpp \
    dbusutility.cpp \
    confmanager.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    xmlutil.h \
    profile.h \
    config.h \
    addprofile.h \
    addstep.h \
    dbusutility.h \
    confmanager.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    addprofile.ui \
    addstep.ui \
    aboutdialog.ui

CONFIG += mobility
MOBILITY = 

symbian {
    TARGET.UID3 = 0xea1d6940
    # TARGET.CAPABILITY += 
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x020000 0x800000
}

OTHER_FILES += \
    config.xml

unix {
  #VARIABLES
  isEmpty(PREFIX) {
    PREFIX = /usr/local
  }
  BINDIR = $$PREFIX/bin
  DATADIR =$$PREFIX/share

  DEFINES += DATADIR=\\\"$$DATADIR\\\" PKGDATADIR=\\\"$$PKGDATADIR\\\"

  #MAKE INSTALL

  INSTALLS += target desktop service iconxpm icon26 icon48 icon64

  target.path =$$BINDIR

  desktop.path = $$DATADIR/applications/hildon
  desktop.files += $${TARGET}.desktop

  service.path = $$DATADIR/dbus-1/services
  service.files += $${TARGET}.service

  iconxpm.path = $$DATADIR/pixmap
  iconxpm.files += ../data/maemo/$${TARGET}.xpm

  icon26.path = $$DATADIR/icons/hicolor/26x26/apps
  icon26.files += ../data/26x26/$${TARGET}.png

  icon48.path = $$DATADIR/icons/hicolor/48x48/apps
  icon48.files += ../data/48x48/$${TARGET}.png

  icon64.path = $$DATADIR/icons/hicolor/64x64/apps
  icon64.files += ../data/64x64/$${TARGET}.png
}
