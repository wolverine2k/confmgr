#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>
#include <QList>
#include <QFile>
#include <QtXml/QDomDocument>
#include "profile.h"

#define APPLICATION_NAME "ConfManager"
#define CONFIG_DIR "/home/user/"

// For test only!
//#define CONFIG_DIR "./"

#define CONFIG_FILE CONFIG_DIR "cfConfig.xml"
#define BACKUP_CONFIG_FILE CONFIG_DIR "cfConfig.bak"

#define DEFAULT_XML "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<Profiles NoOfProfiles=\"0\">\r\n</Profiles>"

class Config : public QObject
{
    Q_OBJECT
public:
    explicit Config(QObject *parent = 0);

    // Close the config file
    ~Config() { closeConfig(); }

    // Return 0 if everything okay else a negative integer
    int addProfile(const Profile &p);
    int removeProfile(const Profile &p);

    // Write back all the changes to the disk
    void flushConfig();

    // Read all the profiles from config file
    bool readAllProfiles();
    // Writes all the profiles from the list to the config file
    // Similar to flushConfig(), flushConfig will internally call
    // writeAllProfiles()
    bool writeAllProfiles();
    // Similar to deletion of config file. Never to be used!
    bool removeAllProfiles();

    // get-set for noOfProfiles
    void setNoOfProfiles(const int &no) { noOfProfiles = no; }
    int getNoOfProfiles() { return noOfProfiles; }

    QList<Profile> profileList;

    bool openConfig();

    // Called by flush and
    bool closeConfig();

private:

    void updateNoOfProfiles();

    QFile confFile;
    QDomDocument domDoc;
    int noOfProfiles;

signals:

public slots:

};

#endif // CONFIG_H
