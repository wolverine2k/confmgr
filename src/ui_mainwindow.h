/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed Nov 17 16:14:19 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAbout;
    QAction *actionAdd_Templated_Profile;
    QWidget *centralWidget;
    QListWidget *mainProfileList;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *mainPBEditProfile;
    QPushButton *mainPBQuit;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btnmainStartConference;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *mainPBAdd;
    QPushButton *mainPBDelete;
    QMenuBar *mainMenuBar;
    QMenu *mainMenuAbout;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 424);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionAdd_Templated_Profile = new QAction(MainWindow);
        actionAdd_Templated_Profile->setObjectName(QString::fromUtf8("actionAdd_Templated_Profile"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        mainProfileList = new QListWidget(centralWidget);
        mainProfileList->setObjectName(QString::fromUtf8("mainProfileList"));
        mainProfileList->setGeometry(QRect(25, 4, 751, 241));
        verticalLayoutWidget_2 = new QWidget(centralWidget);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(535, 260, 241, 141));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        mainPBEditProfile = new QPushButton(verticalLayoutWidget_2);
        mainPBEditProfile->setObjectName(QString::fromUtf8("mainPBEditProfile"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mainPBEditProfile->sizePolicy().hasHeightForWidth());
        mainPBEditProfile->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(mainPBEditProfile);

        mainPBQuit = new QPushButton(verticalLayoutWidget_2);
        mainPBQuit->setObjectName(QString::fromUtf8("mainPBQuit"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(mainPBQuit->sizePolicy().hasHeightForWidth());
        mainPBQuit->setSizePolicy(sizePolicy1);

        verticalLayout_2->addWidget(mainPBQuit);

        horizontalLayoutWidget_2 = new QWidget(centralWidget);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(31, 260, 491, 71));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        btnmainStartConference = new QPushButton(horizontalLayoutWidget_2);
        btnmainStartConference->setObjectName(QString::fromUtf8("btnmainStartConference"));
        sizePolicy.setHeightForWidth(btnmainStartConference->sizePolicy().hasHeightForWidth());
        btnmainStartConference->setSizePolicy(sizePolicy);
        btnmainStartConference->setAutoDefault(true);
        btnmainStartConference->setDefault(true);

        horizontalLayout_2->addWidget(btnmainStartConference);

        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(30, 340, 491, 61));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        mainPBAdd = new QPushButton(horizontalLayoutWidget);
        mainPBAdd->setObjectName(QString::fromUtf8("mainPBAdd"));
        sizePolicy.setHeightForWidth(mainPBAdd->sizePolicy().hasHeightForWidth());
        mainPBAdd->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(mainPBAdd);

        mainPBDelete = new QPushButton(horizontalLayoutWidget);
        mainPBDelete->setObjectName(QString::fromUtf8("mainPBDelete"));
        sizePolicy1.setHeightForWidth(mainPBDelete->sizePolicy().hasHeightForWidth());
        mainPBDelete->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(mainPBDelete);

        MainWindow->setCentralWidget(centralWidget);
        mainMenuBar = new QMenuBar(MainWindow);
        mainMenuBar->setObjectName(QString::fromUtf8("mainMenuBar"));
        mainMenuBar->setGeometry(QRect(0, 0, 800, 25));
        mainMenuAbout = new QMenu(mainMenuBar);
        mainMenuAbout->setObjectName(QString::fromUtf8("mainMenuAbout"));
        MainWindow->setMenuBar(mainMenuBar);

        mainMenuBar->addAction(mainMenuAbout->menuAction());
        mainMenuAbout->addAction(actionAdd_Templated_Profile);
        mainMenuAbout->addAction(actionAbout);

        retranslateUi(MainWindow);
        QObject::connect(mainPBQuit, SIGNAL(clicked()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Conference Manager", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
        actionAdd_Templated_Profile->setText(QApplication::translate("MainWindow", "Add Templated Profile", 0, QApplication::UnicodeUTF8));
        mainPBEditProfile->setText(QApplication::translate("MainWindow", "Edit Profile", 0, QApplication::UnicodeUTF8));
        mainPBQuit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        btnmainStartConference->setText(QApplication::translate("MainWindow", "Start Conference", 0, QApplication::UnicodeUTF8));
        mainPBAdd->setText(QApplication::translate("MainWindow", "Add Profile", 0, QApplication::UnicodeUTF8));
        mainPBDelete->setText(QApplication::translate("MainWindow", "Delete Profile", 0, QApplication::UnicodeUTF8));
        mainMenuAbout->setTitle(QApplication::translate("MainWindow", "About", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
