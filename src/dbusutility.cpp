/*
@version: 0.1
@author: Sudheer K. <scifi.guy@hotmail.com>
@license: GNU General Public License
*/

#include "dbusutility.h"
#include <QDBusMessage>
#include <QDebug>


//Construction is available in the header file due to a peculiar issue with systemBus() function.


//Destructor for Dbus Utility object.
DbusUtility::~DbusUtility(){
    this->connection.disconnectFromBus(this->connection.baseService());
    qDebug() << "Disconnected from system bus";
}

QDBusConnection DbusUtility::getConnection(){
    if (!this->connection.isConnected()){
        qDebug() << "Not connected to Dbus";
    }
    return this->connection;
}

void DbusUtility::setConnection(QDBusConnection connection){
    this->connection = connection;
}

//Utility method to send a dbus signal.
bool DbusUtility::sendSignal(QString strPath,QString strInterface,QString strName){
    QDBusMessage dbusSignal =  QDBusMessage::createSignal(strPath,strInterface,strName);
    bool status = DbusUtility::connection.send(dbusSignal);
    return status;
}

//Utility method to call a dbus method with parameters
bool DbusUtility::sendMethodCall(QString strService,QString strPath,
                                 QString strInterface,QString strMethodName,
                                 QList<QVariant> & arguments){
    QDBusMessage dbusMethodCall = QDBusMessage::createMethodCall(strService,strPath,strInterface,strMethodName);
    dbusMethodCall.setArguments(arguments);
    bool status = DbusUtility::connection.send(dbusMethodCall);
    return status;
}

//Utility method to display a notification (Orange sliding banner in Maemo) with custom message
bool DbusUtility::displayNotification(QString strMessage){
    QList <QVariant> arguments;
    arguments.append(strMessage);

    bool status = this->sendMethodCall(NOTIFICATION_SERVICE,
                                          NOTIFICATION_PATH,
                                          NOTIFICATION_INTERFACE,
                                          QString("SystemNoteInfoprint"),
                                          arguments);
    return status;
}


//Utility method to retrieve the last dbus error
QString DbusUtility::getErrorMessage(){
    QString strErrorMessage;
    QDBusError dbusError = this->connection.lastError();
    if (dbusError.isValid()){
        strErrorMessage = qPrintable(dbusError.message());
    }
    return strErrorMessage;
}
