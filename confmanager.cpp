#include "confmanager.h"
#include <QDebug>
#include <QTimer>

confManager::confManager(QObject *parent) :
    QObject(parent)
{
    mProfileSet = false;
    mInStep = 0;
}

void confManager::setProfile(Profile &p)
{
    mInStep = 0;
    mProfileInUse = p;
    mProfileSet = true;
}

void confManager::continueSendDTMF()
{
    qDebug() << "In ContinueSendDTF for mInStep " << mInStep;
    // We have now waited for the required period of seconds
    // Lets send the DTMF now
    Steps step = mProfileInUse.mSteps.at(mInStep);

    // Increment the steps as we want to point to the next one
    mInStep++;

    QList<QVariant> argsToSend;
    qDebug() << "DTMF = " << step.value();
    argsToSend.append(step.value());
//    argsToSend.append(0);

    bool status = mDBusUtility.sendMethodCall(CSD_SERVICE, CSD_CALL_PATH, CSD_CALL_INTERFACE,
                                         QString("SendDTMF"),argsToSend);

    if(!status)
    {
        qDebug() << "Unable to send DTMF code.";
        QString error = "DBus Error (continueSendDTMF): " + mDBusUtility.getErrorMessage();
        mDBusUtility.displayNotification(error);
    }

    mDBusUtility.displayNotification(step.value());

    // Check if we are over with the sequence or we need to continue
    if(mInStep >= mProfileInUse.mNoOfSteps)
    {
        StopCallMonitors();
        return;
    }

    step = mProfileInUse.mSteps.at(mInStep);
    QTimer *timer = new QTimer(this);
    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(continueSendDTMF()));
    qDebug() << "Setting delay for " << step.delay() << "seconds...";
    timer->start(step.delay()*1000);
}

void confManager::sendDTMF(const QDBusMessage &dBusMessage)
{
    QList<QVariant> listArguments = dBusMessage.arguments();
    bool audioConnect =  listArguments.first().toBool();

    if(mInStep >= mProfileInUse.mNoOfSteps)
    {
        StopCallMonitors();
        return;
    }

    Steps step = mProfileInUse.mSteps.at(mInStep);

    if (audioConnect)
    {
        qDebug() << "Call Active...";
        //Wait for specified delay in the step
        QTimer *timer = new QTimer(this);
        timer->setSingleShot(true);
        connect(timer, SIGNAL(timeout()), this, SLOT(continueSendDTMF()));
        qDebug() << "Setting delay for " << step.delay() << "seconds...";
        timer->start(step.delay()*1000);
    }
}

void confManager::StartCallMonitors()
{
    QDBusConnection connection = mDBusUtility.getConnection();
    /* Declare the slot to be executed when a call is picked up by other party (Audio connection established).
       We need this to confirm whether a call went though successfully.
    */
    bool status = connection.connect(CSD_CALL_SERVICE, CSD_CALL_INSTANCE_PATH, CSD_CALL_INSTANCE_INTERFACE,
                           QString("AudioConnect"),this, SLOT(sendDTMF(const QDBusMessage&)));

    if(!status)
    {
        qDebug() << "Failed to connect to Dbus signal AudioConnect in interface "<< CSD_CALL_INSTANCE_INTERFACE;
        QString error = "DBus Error (StartCallMonitors): " + mDBusUtility.getErrorMessage();
        mDBusUtility.displayNotification(error);
        qDebug() << "Error is: " << error;
    }

    qDebug() << "Successfully connected to Dbus signal AudioConnect in interface "<< CSD_CALL_INSTANCE_INTERFACE;

    /* Declare the slot to be executed when the call is terminated (due to connection errors etc).
       We need this to avoid sending DTMF code on wrong calls.
    */
    status = connection.connect(CSD_CALL_SERVICE, CSD_CALL_INSTANCE_PATH, CSD_CALL_INSTANCE_INTERFACE,
                               QString("Terminated"),this, SLOT(StopCallMonitors()));

    if(!status)
    {
        qDebug() << "Failed to connect to Dbus signal Terminated in interface "<< CSD_CALL_INSTANCE_INTERFACE;
        QString error = "DBus Error (StartCallMonitors): " + mDBusUtility.getErrorMessage();
        mDBusUtility.displayNotification(error);
        qDebug() << "Error is: " << error;
    }

    qDebug() << "Successfully connected to Dbus signal Terminated in interface "<< CSD_CALL_INSTANCE_INTERFACE;

    /* Declare the slot to be executed when a call is received
      (before we can place the call to calling card number).
       It is extremely rare that somebody should get a call within these few seconds.
       In any case, we need this to avoid sending DTMF code on the received call.

       Btw - I don't care for the incoming number here. If anyone is calling the user before we can send DTMF code,
       then we stop sending the DTMF code even if user does not respond to the call.
    */

    status = connection.connect(CSD_CALL_SERVICE, CSD_CALL_PATH, CSD_CALL_INTERFACE,
                               QString("Coming"),this, SLOT(StopCallMonitors()));

    if(!status)
    {
        qDebug() << "Failed to connect to Dbus signal Coming in interface" << CSD_CALL_INTERFACE;
        QString error = "DBus Error (StartCallMonitors): " + mDBusUtility.getErrorMessage();
        mDBusUtility.displayNotification(error);
        qDebug() << "Error is: " << error;
    }
    qDebug() << "Successfully connected to Dbus signal Coming in interface" << CSD_CALL_INTERFACE;
}

void confManager::StopCallMonitors()
{
//    mInStep = 0;
    mProfileSet = false;

    QDBusConnection connection = mDBusUtility.getConnection();

    // Disconnect the slot for audio connection status
    bool status = connection.disconnect(CSD_CALL_BUS_NAME, CSD_CALL_INSTANCE_PATH, CSD_CALL_INSTANCE_INTERFACE,
                                   QString("AudioConnect"),this, SLOT(sendDTMF(const QDBusMessage&)));

    if(!status)
    {
        qDebug() << "Failed to disconnect from Dbus signal AudioConnect in interface" << CSD_CALL_INSTANCE_INTERFACE;
        QString error = "DBus Error (StopCallMonitors): " + mDBusUtility.getErrorMessage();
        mDBusUtility.displayNotification(error);
        qDebug() << "Error is: " << error;
    }
    qDebug() << "Successfully disconnected from Dbus signal AudioConnect in interface "<< CSD_CALL_INSTANCE_INTERFACE;

    // Disconnect the slot for monitoring terminated calls
    status = connection.disconnect(CSD_CALL_BUS_NAME, CSD_CALL_INSTANCE_PATH, CSD_CALL_INSTANCE_INTERFACE,
                                   QString("Terminated"),this, SLOT(StopCallMonitors()));

    if(!status)
    {
        qDebug() << "Failed to disconnect from Dbus signal Terminated in interface" << CSD_CALL_INSTANCE_INTERFACE;
        QString error = "DBus Error (StopCallMonitors): " + mDBusUtility.getErrorMessage();
        mDBusUtility.displayNotification(error);
        qDebug() << "Error is: " << error;
    }
    qDebug() << "Successfully disconnected from Dbus signal Terminated in interface "<< CSD_CALL_INSTANCE_INTERFACE;

    // Disconnect the slot for monitoring incoming calls
    status = connection.disconnect(CSD_CALL_BUS_NAME, CSD_CALL_PATH, CSD_CALL_INTERFACE,
                                   QString("Coming"),this, SLOT(StopCallMonitors()));

    if(!status)
    {
        qDebug() << "Failed to disconnect from Dbus signal Coming in interface" << CSD_CALL_INTERFACE;
        QString error = "DBus Error (StopCallMonitors): " + mDBusUtility.getErrorMessage();
        mDBusUtility.displayNotification(error);
        qDebug() << "Error is: " << error;
    }
    qDebug() << "Successfully disconnected from Dbus signal Coming in interface" << CSD_CALL_INTERFACE;
}

void confManager::startConference()
{
    mInStep = 0;
    if(!mProfileSet)
    {
        qDebug() << "Please set the profile to use with Conference Manager first!";
        return;
    }
    //Assume that the first number is always a phone number...
    Steps step = mProfileInUse.mSteps.at(mInStep);
    mInStep++;
    QList<QVariant> sendArgs;
    sendArgs.append(step.value());
    sendArgs.append(0);
    bool status = mDBusUtility.sendMethodCall(CSD_SERVICE, CSD_CALL_PATH, CSD_CALL_INTERFACE,
                                              QString("CreateWith"), sendArgs);
    if(!status)
    {
        QString error = "Error while dialing: " + mDBusUtility.getErrorMessage();
        qDebug() << error;
        mDBusUtility.displayNotification(error);
        return;
    }
    StartCallMonitors();
}
