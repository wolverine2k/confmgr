/********************************************************************************
** Form generated from reading UI file 'addprofile.ui'
**
** Created: Mon Aug 9 23:18:37 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDPROFILE_H
#define UI_ADDPROFILE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddProfile
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *addProAddStep;
    QPushButton *addProModifyStep;
    QPushButton *addProRemoveStep;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *addProSave;
    QPushButton *addProCancel;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *addProName;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QListWidget *addProStepList;

    void setupUi(QWidget *AddProfile)
    {
        if (AddProfile->objectName().isEmpty())
            AddProfile->setObjectName(QString::fromUtf8("AddProfile"));
        AddProfile->setWindowModality(Qt::ApplicationModal);
        AddProfile->resize(800, 424);
        AddProfile->setFocusPolicy(Qt::TabFocus);
        verticalLayoutWidget = new QWidget(AddProfile);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(560, 80, 211, 231));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        addProAddStep = new QPushButton(verticalLayoutWidget);
        addProAddStep->setObjectName(QString::fromUtf8("addProAddStep"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(addProAddStep->sizePolicy().hasHeightForWidth());
        addProAddStep->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(addProAddStep);

        addProModifyStep = new QPushButton(verticalLayoutWidget);
        addProModifyStep->setObjectName(QString::fromUtf8("addProModifyStep"));
        sizePolicy.setHeightForWidth(addProModifyStep->sizePolicy().hasHeightForWidth());
        addProModifyStep->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(addProModifyStep);

        addProRemoveStep = new QPushButton(verticalLayoutWidget);
        addProRemoveStep->setObjectName(QString::fromUtf8("addProRemoveStep"));
        sizePolicy.setHeightForWidth(addProRemoveStep->sizePolicy().hasHeightForWidth());
        addProRemoveStep->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(addProRemoveStep);

        horizontalLayoutWidget = new QWidget(AddProfile);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(30, 330, 741, 71));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        addProSave = new QPushButton(horizontalLayoutWidget);
        addProSave->setObjectName(QString::fromUtf8("addProSave"));
        sizePolicy.setHeightForWidth(addProSave->sizePolicy().hasHeightForWidth());
        addProSave->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(addProSave);

        addProCancel = new QPushButton(horizontalLayoutWidget);
        addProCancel->setObjectName(QString::fromUtf8("addProCancel"));
        sizePolicy.setHeightForWidth(addProCancel->sizePolicy().hasHeightForWidth());
        addProCancel->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(addProCancel);

        horizontalLayoutWidget_2 = new QWidget(AddProfile);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(30, 10, 741, 61));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(horizontalLayoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        addProName = new QLineEdit(horizontalLayoutWidget_2);
        addProName->setObjectName(QString::fromUtf8("addProName"));

        horizontalLayout_2->addWidget(addProName);

        verticalLayoutWidget_2 = new QWidget(AddProfile);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(30, 80, 511, 231));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(verticalLayoutWidget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        addProStepList = new QListWidget(verticalLayoutWidget_2);
        addProStepList->setObjectName(QString::fromUtf8("addProStepList"));

        verticalLayout_2->addWidget(addProStepList);


        retranslateUi(AddProfile);
        QObject::connect(addProCancel, SIGNAL(clicked()), AddProfile, SLOT(close()));
        QObject::connect(addProAddStep, SIGNAL(clicked()), AddProfile, SLOT(showStepsUI()));

        QMetaObject::connectSlotsByName(AddProfile);
    } // setupUi

    void retranslateUi(QWidget *AddProfile)
    {
        AddProfile->setWindowTitle(QApplication::translate("AddProfile", "AddProfile", 0, QApplication::UnicodeUTF8));
        addProAddStep->setText(QApplication::translate("AddProfile", "Add Step", 0, QApplication::UnicodeUTF8));
        addProModifyStep->setText(QApplication::translate("AddProfile", "Modify Step", 0, QApplication::UnicodeUTF8));
        addProRemoveStep->setText(QApplication::translate("AddProfile", "Remove Step", 0, QApplication::UnicodeUTF8));
        addProSave->setText(QApplication::translate("AddProfile", "Save Profile", 0, QApplication::UnicodeUTF8));
        addProCancel->setText(QApplication::translate("AddProfile", "Cancel Operation", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AddProfile", "Profile Name:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("AddProfile", "Steps:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AddProfile: public Ui_AddProfile {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDPROFILE_H
