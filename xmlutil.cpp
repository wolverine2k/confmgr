#include "xmlutil.h"
#include <QDebug>

Xmlutil::Xmlutil(QObject *parent) :
    QObject(parent)
{
}

QDomElement Xmlutil::addElement(QDomDocument &doc, QDomNode &node,
                       const QString &tag, const QString &value)
{
    QDomElement element = doc.createElement(tag);
    node.appendChild(element);
    if(!value.isNull())
    {
        QDomText text = doc.createTextNode(value);
        element.appendChild(text);
    }
    return element;
}

/*
// Unit test from main

    Steps step("+4670", 1);
    int error = 0;

    Profile p;
    p.mName = "Diva";
    p.mNoOfSteps = 2;
    p.mSteps.append(step);
    step.setDelay(2);
    step.setValue("+4788");
    p.mSteps.append(step);

    qDebug() << p.generateProfileXML(p, error);

*/
QString Xmlutil::generateProfileXML(const Profile &profile, int *error,
                                    QDomDocument *doc, QDomElement *elem)
{
    QDomDocument *pDoc = NULL;
    if(0 == doc)
        pDoc = new QDomDocument;
    else
        pDoc = doc;

    QDomElement domElement;

    if(0 == elem)
        domElement = Xmlutil::addElement(*pDoc, *pDoc, PROFILE_ELEM_TAG);
    else
        domElement = Xmlutil::addElement(*pDoc, *elem, PROFILE_ELEM_TAG);

    domElement.setAttribute(NAME_ATTR, profile.mName);
    domElement.setAttribute(NO_OF_STEPS_ATTR, profile.mNoOfSteps);

    if(profile.mNoOfSteps > 0)
    {
        for(unsigned int i = 0; i < profile.mNoOfSteps; i++)
        {
            QDomElement stepEl = Xmlutil::addElement(*pDoc, domElement, STEP_TAG);
            Steps step = profile.mSteps.at(i);
            stepEl.setAttribute(VALUE_ATTR, step.value());
            stepEl.setAttribute(DELAY_ATTR, step.delay());
        }
    }

    QString xmlString = pDoc->toString();

    if(0 == doc)
        delete pDoc;

    return xmlString;
}

/*
QString generateXML()
{
    Steps step("+4670", 1);
    Profile p;
    p.mName = "Diva";
    p.mNoOfSteps = 2;
    p.mSteps.append(step);
    step.setDelay(2);
    step.setValue("+4788");
    p.mSteps.append(step);

    return xmlutil::generateProfileXML(p);
}

// Unit test from main...

    QString xmlString = generateXML();
    Profile p;

    int e = xmlutil::degenerateProfileXML(xmlString, p);
*/

int Xmlutil::degenerateProfileXML(const QString xmlString, Profile &profile,
                                  QDomElement *pElem)
{
    QDomDocument doc;
    Steps step;
    QDomElement *elem = NULL;
    if(0 == pElem && !xmlString.isNull())
    {
        doc.setContent(xmlString);
        *elem = doc.namedItem(PROFILE_ELEM_TAG).toElement();
    }
    else
    {
        elem = pElem;
    }

    if(elem->isNull())
        return -1;

    profile.mName = elem->attribute(NAME_ATTR);
    profile.mNoOfSteps = elem->attribute(NO_OF_STEPS_ATTR).toInt();

    qDebug() << profile.mName << ":" << profile.mNoOfSteps;
    QDomElement child = elem->firstChild().toElement();

    profile.mSteps.clear();

    for(unsigned int i = 0; i < profile.mNoOfSteps ; i++)
    {
        step.setDelay(child.attribute(DELAY_ATTR).toInt());
        step.setValue(child.attribute(VALUE_ATTR));
        profile.mSteps.append(step);
        qDebug() << step.value() << ":" << step.delay();
        child = child.nextSiblingElement();
    }
    return 0;
}
