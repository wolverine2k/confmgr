#ifndef ADDSTEP_H
#define ADDSTEP_H

#include <QWidget>
#include "profile.h"

namespace Ui {
    class AddStep;
}

class AddStep : public QWidget
{
    Q_OBJECT

public:
    explicit AddStep(QWidget *parent = 0);
    ~AddStep();

    void clear();
    void showStep(Steps &step);

signals:
    void StepAddedSuccessfully(Steps step);

public slots:
    void AddOperationStep();

private:
    Ui::AddStep *ui;
    Steps mStep;
};

#endif // ADDSTEP_H
