#ifndef XMLUTIL_H
#define XMLUTIL_H

#include <QObject>
#include <QtXml/QDomElement>
#include "profile.h"

#define NO_OF_PROFILE_ATTR "NoOfProfiles"
#define PROFILE_TAG "Profiles"
#define PROFILE_ELEM_TAG "Profile"
#define NAME_ATTR "Name"
#define NO_OF_STEPS_ATTR "NoOfSteps"
#define STEP_TAG "Step"
#define VALUE_ATTR "Value"
#define DELAY_ATTR "Delay"

class Xmlutil : public QObject
{
    Q_OBJECT
public:
    explicit Xmlutil(QObject *parent = 0);

    static QDomElement addElement(QDomDocument &doc, QDomNode &node,
                           const QString &tag,
                           const QString &value = QString::null);

    static QString generateProfileXML(const Profile &profile, int *error = 0,
                                      QDomDocument *doc = 0, QDomElement *elem = 0);
    // returns non-zero on error
    static int degenerateProfileXML(const QString xmlString, Profile &profile,
                                    QDomElement *pElem = 0);

//    QDomElement removeElement
signals:

public slots:

};

/*
    QDomDocument doc("mydocument");
    QFile file(argv[1]);
    if (!file.open(QIODevice::ReadOnly))
        return -1;
    if (!doc.setContent(&file)) {
        file.close();
        return -1;
    }
    file.close();

// print out the element names of all elements that are direct children
// of the outermost element.
    QDomElement docElem = doc.namedItem("Profiles").toElement();
    qDebug() << "Main Tag: " << docElem.attribute("NoOfProfiles") << endl;

    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
            qDebug() << "Name" << qPrintable(e.tagName()) << e.attribute("Name") << endl; // the node really is an element.
            qDebug() << "NoOfSteps" << qPrintable(e.tagName()) << e.attribute("NoOfSteps") << endl; // the node really is an element.
        }
        QDomNode node = n.firstChild();
        for(int i = 0; i < e.attribute("NoOfSteps").toInt(); i++)
        {
            QDomElement elem = node.toElement();
            if(!elem.isNull())
            {
                qDebug() << "Value" << qPrintable(elem.tagName()) << elem.attribute("Value") << endl; // the node really is an element.
                qDebug() << "Delay" << qPrintable(elem.tagName()) << elem.attribute("Delay") << endl; // the node really is an element.
            }
            node = node.nextSibling();
        }
        n = n.nextSibling();
    }

 */

#endif // XMLUTIL_H
