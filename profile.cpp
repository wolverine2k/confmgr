#include "profile.h"
#include "xmlutil.h"
#include <QtXml/QDomDocument>
#include <QtDebug>

Profile::Profile(QObject *parent) :
    QObject(parent)
{
    mSteps.clear();
    mNoOfSteps = 0;
}

Profile::Profile(const Profile &p) :
    QObject(p.parent())
{
    *this = p;
}

Profile::~Profile()
{
    mSteps.clear();
}

Profile& Profile::operator =(const Profile &p)
{
    if(this != &p)
    {
        mXMLString = p.mXMLString;
        mName = p.mName;
        mNoOfSteps = p.mNoOfSteps;
        mSteps.clear();
        mSteps = p.mSteps;
    }
    return *this;
}
