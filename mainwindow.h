#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include "config.h"
#include "addprofile.h"
#include "confmanager.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

#if defined(Q_WS_MAEMO_5)
    void setPortraitMode() { setAttribute(Qt::WA_Maemo5PortraitOrientation, true); }
#endif

    void Initialize();

public slots:
    void updateProfileList(Profile p);

private:
    Ui::MainWindow *ui;
    Config mConfig;
    AddProfile mFrmAddProfile;
    bool bIsProfileEdited;
    confManager mConfMgr;

private slots:
    void on_actionAdd_Templated_Profile_triggered();
    void on_actionAbout_triggered();
    void on_mainPBQuit_clicked();
    void on_mainPBEditProfile_clicked();
    void on_btnmainStartConference_clicked();
    void on_mainPBDelete_clicked();
    void on_mainPBAdd_clicked();
};

#endif // MAINWINDOW_H
