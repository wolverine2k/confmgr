#include "addprofile.h"
#include "ui_addprofile.h"
#include <QDebug>
#include <QMessageBox>

AddProfile::AddProfile(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddProfile)
{
    ui->setupUi(this);
    connect(&mFrmAddStep, SIGNAL(StepAddedSuccessfully(Steps)),
            this, SLOT(updateStepList(Steps)));
    mFrmAddStep.setWindowFlags(mFrmAddStep.windowFlags() | Qt::Window);
    bIsStepEdited = false;
    iStepEditPosition = 0;
    mFrmAddStep.setParent(this, Qt::Window);
    mFrmAddStep.setAttribute(Qt::WA_Maemo5StackedWindow);
}

AddProfile::~AddProfile()
{
    delete ui;
}

void AddProfile::on_addProCancel_clicked()
{
    this->close();
}

void AddProfile::on_addProSave_clicked()
{
    Profile p;
    p.mName = ui->addProName->text();
    p.mNoOfSteps = ui->addProStepList->count();
    p.mSteps = mSteps;
    emit(ProfileAddedSuccessfully(p));
    this->close();
}

void AddProfile::showStepsUI()
{
    mFrmAddStep.setParent(this, Qt::Window);
    mFrmAddStep.clear();
    mFrmAddStep.setAttribute(Qt::WA_Maemo5StackedWindow);
    mFrmAddStep.show();
}

void AddProfile::updateStepList(Steps step)
{
    QString text = "Value: " + step.value();
    text += " || Delay: " + QString::number(step.delay());
    if(bIsStepEdited)
    {
        // Reset the flag
        bIsStepEdited = false;

        // Remove the old one...
        mSteps.removeAt(iStepEditPosition);
        QString *pText = (QString*) ui->addProStepList->takeItem(ui->addProStepList->currentRow());
        delete pText;

        // update with the new one...
        mSteps.insert(iStepEditPosition, step);
        ui->addProStepList->insertItem(iStepEditPosition, text);
    }
    else
    {
        mSteps.append(step);
        ui->addProStepList->addItem(text);
    }
    qDebug() << "updateStepList(): Text in List:  " << text;
}

void AddProfile::on_addProRemoveStep_clicked()
{
    if(ui->addProStepList->count() <= 0 || ui->addProStepList->currentRow() < 0)
    {
        QMessageBox msg;
        msg.setText("Please select a step first!");
        msg.exec();
        return;
    }

    mSteps.removeAt(ui->addProStepList->currentRow());
    QString *pText = (QString*) ui->addProStepList->takeItem(ui->addProStepList->currentRow());
    delete pText;
}

void AddProfile::clear()
{
    mSteps.clear();
    ui->addProStepList->clear();
    ui->addProName->setText(QString::null);
}

void AddProfile::showProfile(Profile &p)
{
    ui->addProName->setText(p.mName);
    mSteps.clear();
    mSteps = p.mSteps;
    for(unsigned int i = 0; i < p.mNoOfSteps; i++)
    {
        Steps step = p.mSteps.at(i);
        QString text = "Value: " + step.value();
        text += " || Delay: " + QString::number(step.delay());
        ui->addProStepList->addItem(text);
    }
    this->show();
}

void AddProfile::on_addProModifyStep_clicked()
{
    if(ui->addProStepList->count() <= 0 || ui->addProStepList->currentRow() < 0)
    {
        QMessageBox msg;
        msg.setText("Please add/select a step first!");
        msg.exec();
        return;
    }
    bIsStepEdited = true;
    iStepEditPosition = ui->addProStepList->currentRow();
    qDebug() << "Step to be edited: " << iStepEditPosition;
    mFrmAddStep.setParent(this, Qt::Window);
    mFrmAddStep.setAttribute(Qt::WA_Maemo5StackedWindow);
    mFrmAddStep.showStep((Steps &)mSteps.at(iStepEditPosition));
}
