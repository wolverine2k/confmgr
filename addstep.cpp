#include "addstep.h"
#include "ui_addstep.h"

AddStep::AddStep(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddStep)
{
    ui->setupUi(this);
}

AddStep::~AddStep()
{
    delete ui;
}

void AddStep::AddOperationStep()
{
    mStep.setDelay(ui->addStepDelay->text().toInt());
    mStep.setValue(ui->addStepValue->text());
    emit(StepAddedSuccessfully(mStep));
    this->close();
}

void AddStep::clear()
{
    ui->addStepDelay->setText(QString::null);
    ui->addStepValue->setText(QString::null);
}

void AddStep::showStep(Steps &step)
{
    mStep = step;
    clear();
    ui->addStepDelay->setText(QString::number(mStep.delay()));
    ui->addStepValue->setText(mStep.value());
    this->show();
}
