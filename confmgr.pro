#-------------------------------------------------
#
# Project created by QtCreator 2010-06-23T14:10:47
#
#-------------------------------------------------

QT       += core gui
QT       += xml maemo5 dbus

TARGET = confmgr
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    xmlutil.cpp \
    profile.cpp \
    config.cpp \
    addprofile.cpp \
    addstep.cpp \
    dbusutility.cpp \
    confmanager.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    xmlutil.h \
    profile.h \
    config.h \
    addprofile.h \
    addstep.h \
    dbusutility.h \
    confmanager.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    addprofile.ui \
    addstep.ui \
    aboutdialog.ui

CONFIG += mobility
MOBILITY = 

symbian {
    TARGET.UID3 = 0xea1d6940
    # TARGET.CAPABILITY += 
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x020000 0x800000
}

OTHER_FILES += \
    config.xml
