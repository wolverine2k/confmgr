#ifndef ADDPROFILE_H
#define ADDPROFILE_H

#include <QWidget>
#include <QList>
#include "addstep.h"
#include "profile.h"

namespace Ui {
    class AddProfile;
}

class AddProfile : public QWidget
{
    Q_OBJECT

public:
    explicit AddProfile(QWidget *parent = 0);
    ~AddProfile();

    void clear();

public slots:
    void updateStepList(Steps step);
    void showProfile(Profile &p);

private:
    Ui::AddProfile *ui;
    QList<Steps> mSteps;
    AddStep mFrmAddStep;
    bool bIsStepEdited;
    int iStepEditPosition;

signals:
    void ProfileAddedSuccessfully(Profile profile);

private slots:
    void on_addProModifyStep_clicked();
    void on_addProRemoveStep_clicked();
    void on_addProSave_clicked();
    void on_addProCancel_clicked();
    void showStepsUI();
};

#endif // ADDPROFILE_H
