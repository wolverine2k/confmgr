#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "xmlutil.h"
#include "aboutdialog.h"
#include <QDebug>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(&mFrmAddProfile, SIGNAL(ProfileAddedSuccessfully(Profile)),
            this, SLOT(updateProfileList(Profile)));
/*
    connect(&ui->mainMenuAbout, SIGNAL(Triggered(QAction*)),
            this, SLOT(mainAboutClickedSlot()));
*/
    Initialize();
}

MainWindow::~MainWindow()
{
    mConfig.writeAllProfiles();
    mConfig.closeConfig();
    delete ui;
}

void MainWindow::Initialize()
{
     mConfig.openConfig();
     for(int i = 0; i < mConfig.getNoOfProfiles(); i++)
     {
         Profile p = mConfig.profileList.at(i);
         QString text = "Name: " + p.mName + " || Steps: " + QString::number(p.mNoOfSteps);
         ui->mainProfileList->addItem(text);
     }
     ui->centralWidget->setAttribute(Qt::WA_Maemo5StackedWindow);
     mFrmAddProfile.setWindowFlags(mFrmAddProfile.windowFlags() | Qt::Window);
}

void MainWindow::on_mainPBAdd_clicked()
{
    bIsProfileEdited = false;
    mFrmAddProfile.setParent(this, Qt::Window);
    mFrmAddProfile.clear();
    mFrmAddProfile.setAttribute(Qt::WA_Maemo5StackedWindow);
    mFrmAddProfile.show();
}

void MainWindow::updateProfileList(Profile p)
{
    if(bIsProfileEdited)
    {
        // Delete previous profile and write this new one
        QString *pText = (QString *) ui->mainProfileList->takeItem(ui->mainProfileList->currentRow());
        delete pText;
        mConfig.removeProfile(p);
    }
    qDebug() << Xmlutil::generateProfileXML(p);
    mConfig.addProfile(p);
    QString text = "Name: " + p.mName + " || Steps: " + QString::number(p.mNoOfSteps);
    ui->mainProfileList->addItem(text);
}

void MainWindow::on_mainPBDelete_clicked()
{
    if(ui->mainProfileList->count() <= 0 || ui->mainProfileList->currentRow() < 0)
    {
        QMessageBox msg;
        msg.setText("Please select a profile first!");
        msg.exec();
        return;    
    }

    Profile p = mConfig.profileList.at(ui->mainProfileList->currentRow());
    qDebug() << "Profile to be deleted: " << endl << Xmlutil::generateProfileXML(p);
    QString *pText = (QString *) ui->mainProfileList->takeItem(ui->mainProfileList->currentRow());
    delete pText;
    mConfig.removeProfile(p);
}

void MainWindow::on_btnmainStartConference_clicked()
{
    if(ui->mainProfileList->count() <= 0 || ui->mainProfileList->currentRow() < 0)
    {
        QMessageBox msg;
        msg.setText("Please select a profile first!");
        msg.exec();
        return;
    }
    Profile p = mConfig.profileList.at(ui->mainProfileList->currentRow());
    qDebug() << "Profile To Dial: " << endl << Xmlutil::generateProfileXML(p);
    mConfMgr.setProfile(p);
    mConfMgr.startConference();
}

void MainWindow::on_mainPBEditProfile_clicked()
{
    if(ui->mainProfileList->count() <= 0 || ui->mainProfileList->currentRow() < 0)
    {
        QMessageBox msg;
        msg.setText("Please select a profile first!");
        msg.exec();
        return;
    }
    bIsProfileEdited = true;
    Profile p = mConfig.profileList.at(ui->mainProfileList->currentRow());
    qDebug() << "Profile to be Edited: " << endl << Xmlutil::generateProfileXML(p);
    mFrmAddProfile.setParent(this, Qt::Window);
    mFrmAddProfile.setAttribute(Qt::WA_Maemo5StackedWindow);
    mFrmAddProfile.clear();
    mFrmAddProfile.showProfile(p);
}

void MainWindow::on_mainPBQuit_clicked()
{
    mConfig.writeAllProfiles();
    mConfig.closeConfig();
    close();
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog abtdlg;
    abtdlg.exec();
}

void MainWindow::on_actionAdd_Templated_Profile_triggered()
{
    QMessageBox msg;
    msg.setText("Placeholder for a nice function yet to be implemented!");
    msg.exec();
    return;
}
